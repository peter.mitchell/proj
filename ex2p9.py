import numpy as np

# My soln
billboard_set1 = np.linspace(17,28,3)
billboard_set2 = np.linspace(32,36,3)

rest = 30

distC = [abs(billboard_set1[2]-rest), abs(billboard_set2[0]-rest)]
print distC

# Course soln - doesn't work with this version of numpy
#dists = np.abs(np.linspace([17,32], [28,36], num = 3, axis=1) - 30)
#distC = dists[[0, 1], [2, 0]]

#print distC
