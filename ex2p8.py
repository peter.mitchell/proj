import numpy as np

np.random.seed(5555)
gold = np.random.randint(low=0, high=10, size=(7,7))
print gold

locs = np.array([[0,4],
                 [2,2],
                 [2,3],
                 [5,1],
                 [6,3]])

print locs

found = np.sum(gold[locs[:,0], locs[:,1]])
print found

# After some head-scratching - I got to the same soln as the course, nice!
