import numpy as np

score_diffs = np.round(np.random.uniform(low=-15, high=15, size=(10,10)),2)

np.fill_diagonal(score_diffs, np.nan)

score_diffs[np.triu_indices(10, k=1)] = -score_diffs[np.tril_indices(10, k=-1)]

print score_diffs

# Find the 5 best matchups with the lowest sum of squared difference differentials

id_i = np.arange(0,10)[:,None] * np.ones(10)[None,:].astype("int")
id_j = np.arange(0,10)[None,:] * np.ones(10)[:,None].astype("int")

sd = score_diffs[np.triu_indices(10)]
id_i = id_i[np.triu_indices(10)]
id_j = id_j[np.triu_indices(10)]

ok = np.isnan(sd)==False
sd = sd[ok]; id_i = id_i[ok]; id_j = id_j[ok]

order = np.argsort(np.square(sd))

print ""
print sd[order]
print id_i[order]
print id_j[order]

# At this point I'm not sure whether I'm supposed to find 5 best matchups using all 10 players, or whether I can duplicate players
# Latter is solved by above. Former I don't know how to do without invoking a loop

# Question was asking for the former. The solution was to build an array of all possible matchup combinations
# i.e. 1v2, 3v4, 5v6, 7v8, 9v10 is one possible set of matchups
# Then compute the summed difference of each possible set of allowed matchups, and pick the miniumum

# Ironically, my method, with a tiny bit of looping at the end
# Would have been much faster than the course soln, since my method does not require building enormous 2d arrays of all possible permutations!

from itertools import permutations
perms = np.array(list(permutations([0,1,2,3,4,5,6,7,8,9])))

print ""
print "Course solution"
print ""
print perms
print perms.shape

# Partition into two matrices representing player 1 and player 2
# i.e. index 0 plays index 1, index 2 plays index 3, and so on
p1 = perms[:, [0,2,4,6,8]]
p2 = perms[:, [1,3,5,7,9]]

# Only retain matchups where player 1 < player 2
# i.e. remove duplicate matchup sets, and also remove matchups where one player plays themself
keeps = np.all(p1 < p2, axis=1)
p1 = p1[keeps]
p2 = p2[keeps]


# Build a matrix where (i,j) gives the expected point differential
# for the jth pairing in the ith schedule
#print p1.shape, p2.shape, score_diffs.shape
point_diffs = score_diffs[p1, p2]

# Calculate sum of squared point differentials
schedule_scores = np.sum(np.square(point_diffs), axis=1)

# Identify the best schedule
best_idx = np.argmin(schedule_scores)

# Format nicely to print the answer
best_schedule = np.vstack((p1[best_idx], p2[best_idx]))

print best_schedule
