import numpy as np

locs = np.array([
    [0,0,0],
    [1,1,2],
    [0,0,0],
    [2,1,3],
    [5,5,4],
    [5,0,0],
    [5,0,0],
    [0,0,0],
    [2,1,3],
    [1,3,1]
    ])

weights = np.abs(np.random.randn(10))

inverse_order = np.argsort(weights)[::-1]
locs_u, inverse_index = np.unique(locs[inverse_order],axis=0,return_index=True)

index = inverse_index

print locs
print weights

print weights[inverse_order][index]
