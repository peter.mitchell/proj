import numpy as np

jim = np.round(np.random.normal(loc=100, scale=5, size=(5,2,4)))

blocks = np.array([
            [[0,2],[1,3]],
            [[1,2],[0,0]],
            [[0,0],[1,2]],
            [[1,1],[0,3]],
            [[0,1],[1,0]]
            ])

pops = np.array([
    [100, 105],
    [110, 92],
    [95, 99],
    [89, 107],
    [101, 98]
])

blocks_x1 = blocks[:,0,0]
blocks_y1 = blocks[:,0,1]
blocks_x2 = blocks[:,1,0]
blocks_y2 = blocks[:,1,1]

jim1 = np.diag(jim[:,blocks_x1, blocks_y1])
jim2 = np.diag(jim[:,blocks_x2,blocks_y2])

n1 = len(np.where( (jim1>pops[:,0]*1.1) | (jim1<pops[:,0]/1.1) )[0])
n2 = len(np.where( (jim2>pops[:,1]*1.1) | (jim2<pops[:,1]/1.1) )[0])
print n1+n2

# Course soln
print np.arange(5)[:,None].shape, blocks[:,:,0].shape, blocks[:,:,1].shape
jim_pops = jim[np.arange(5)[:, None], blocks[:, :, 0], blocks[:, :, 1]]
print jim_pops.shape
print np.sum(np.abs(pops - jim_pops)/pops >= 0.10)
