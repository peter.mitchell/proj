import numpy as np

# Row is game, column is round, value is door index
prize_doors = np.array([
    [1, 0, 2],
    [0, 0, 1],
    [3, 3, 1],
    [1, 2, 0],
    [2, 1, 1]
])

prizes = np.array([
    [100, 150, 500],
    [200, 300, 250],
    [150, 100, 325],
    [425, 200, 100],
    [200, 250, 300]
])


prizes_full = np.zeros((5,3,4))

game = np.arange(5)[:,None]
round = np.arange(3)[None,:]

# The basic rule here is that if you do a multi-dimensional index, where dimension is a separate array
# What you get out has the shape of each of those separate index arrays, provided they have the same shape
# If they don't numpy will attemp to broadcast them into the same shape

# So in this case, first dimension is the game id, which we setup as a 5x1 array
# The second dimension is the game round id, which we setup as a 1x3 array
# These broadcast together to form a 5x3 array
# This now has the same shape as the prize_doors id array, so finally we get out what we want

prizes_full[game,round,prize_doors] = prizes

print prizes_full

# And hey this one actually works! (i.e. prizes_full[game,round,prize_doors] was a view of prizes_full, not a copy)

