import numpy as np

field = np.zeros(shape = (10, 10))
field_flat = np.ravel(field)
field_flat[0:20] += np.round(np.random.randn(20),2)
np.random.shuffle(field_flat)
field = field_flat.reshape(10,10)

print field
