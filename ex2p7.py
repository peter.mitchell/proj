import numpy as np

dailywts = 185 - np.arange(5.*7)/5
print dailywts

# My blind solution
re = np.reshape(dailywts, (5,7))
av_weekend = np.sum(re[:,5:], axis=1) / 2
print "My solution"
print av_weekend

# Course solution
# [5::7] asks for element 5 out of every 7 elements
av_weekend = (dailywts[5::7] + dailywts[6::7])/2
print "Course solution"
print av_weekend
