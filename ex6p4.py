import numpy as np

rain_locations = np.array([
        [2,3],
        [0,1],
        [2,2],
        [2,3],
        [1,1],
        [2,3],
        [1,1]
        ])

rain_amounts = np.array([0.5,1.1,0.2,0.9,1.3,0.4,2.0])

id_for_sort = rain_locations[:,0]*10 +rain_locations[:,1]
order = np.argsort(id_for_sort)

rain_locations = rain_locations[order]
rain_amounts = rain_amounts[order]

amount_cumul = np.cumsum(rain_amounts)

rain_loc_u, index = np.unique(rain_locations, axis=0, return_index=True)

amount_cumul1 = np.append(amount_cumul[index[1:]-1], amount_cumul[-1])
amount_cumul2 = np.append(amount_cumul[0], amount_cumul1[1:] - amount_cumul1[0:-1])

rain_grid = np.zeros(shape=(3,4))

rain_grid[rain_loc_u[:,0],rain_loc_u[:,1]] += amount_cumul2

print rain_grid


# Course solution, (LOL!) # Uses a universal function ".at()" to modify the behaviour of the np.add function
rainfall = np.zeros((3,4))
np.add.at(rainfall, (rain_locations[:, 0], rain_locations[:, 1]), rain_amounts)

print rainfall.shape, (rain_locations[:, 0], rain_locations[:, 1]), rain_amounts.shape

print rainfall
