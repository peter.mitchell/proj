import numpy as np

# Note np.round rounds to desired no of decimal places, neat
love_scores = np.round(np.random.random_sample(10) * 100,2)

diff_matrix = np.abs( love_scores[:,None] - love_scores[None,:])
print diff_matrix
