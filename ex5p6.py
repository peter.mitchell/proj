import numpy as np

peanut = np.zeros(shape = (4, 5))
print peanut

butter = np.array([3, 0, 4, 1])
print butter

# Oops misunderstood the question
#peanut[np.arange(4),butter] = 1
#print peanut

# Course soln
a = butter[:,None]
b = np.arange(5)

# with the "a" trick, the code compares element 1 of butter "3" against the column indexes of the peanut array "b"
result = (a <= b).astype("int")

print result
