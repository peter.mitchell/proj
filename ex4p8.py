import numpy as np

ratings = np.round(np.random.random_sample((10,2))*10)
ratings[[1,2,7,9], [0,0,0,0]] = np.nan

overall = np.copy(ratings[:,0])
no_score = np.isnan(overall)
overall[no_score] = ratings[:,1][no_score]
ratings = np.concatenate((ratings,overall[:,None]),axis=1)

# smarter method
ratings = np.round(np.random.random_sample((10,2))*10)
ratings[[1,2,7,9], [0,0,0,0]] = np.nan

overall = np.where(np.isnan(ratings[:,0]), ratings[:,1], ratings[:,0])
ratings = np.concatenate((ratings,overall[:,None]),axis=1)
