import numpy as np

scores = np.random.random_sample(15)*70 + 30

oh_dear = np.where(scores < 60)[0]
if len(oh_dear) > 3:
    oh_dear = oh_dear[0:3]

scores[oh_dear] = 0

print scores
