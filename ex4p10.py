import numpy as np

schedules = np.random.random_integers(0,5,size=(1000,21))

print schedules

diff = schedules[:,1:] - schedules[:,0:-1]
diff = np.where(diff!=0, diff*0, np.ones_like(diff))
sum = np.sum(diff,axis=1)

sched_index_ok = np.where(sum==0)[0]

print sched_index_ok

# Course solution
is_valid = np.all(schedules[:, :-1] != schedules[:, 1:], axis=1)
sched_index_ok = np.nonzero(is_valid)[0]
print sched_index_ok
