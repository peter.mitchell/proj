import numpy as np

chewy = np.zeros((10,10,10))

odd = np.where(np.mod(np.arange(10),2) == 1)[0]
even = np.where(odd == False)[0]

prime = np.where([True, True, True, False, True, False, True, False, False, False])[0]

# My soln
chewy[odd][:,even][:,:,prime] = 1

# Course soln
chewy[odd[:,None,None],even[None,:,None],prime[None,None]] = 1

# Neither works in python 2 because in both cases the returned index array is a copy, not a "view" of the original
# In practice, you would have to do the standard one set of indexes at a time hack soln

# Course soln2
chewy[np.ix_(odd,even,prime)] = 1
#print chewy
# Lol this one doesn't work either - I'm guessing this behaviour changed in python3
