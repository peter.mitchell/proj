import numpy as np

A = np.arange(10*3).reshape((10,3))
#print(A)
## [[ 0  1  2]
##  [ 3  4  5]
##  [ 6  7  8]
##  [ 9 10 11]
##  [12 13 14]
##  [15 16 17]
##  [18 19 20]
##  [21 22 23]
##  [24 25 26]
##  [27 28 29]]
B = np.arange(10*5).reshape((10,5))
#print(B)


# My attempt (turned out to be wrong)
'''answer = np.multiply.outer(A,B)
answer = np.product(answer,axis=2)

print answer.shape
print answer'''

# Course soln 1
answer2 = A[...,None]*B[:,None]
# It turns out that "..." as indexing slice is a multidimensional version of ":", and just does ":" for each dimension you don't otherwise specify

answer3 = A[::,::,None]*B[:,None,:]
# Which is the same as

print ""
print answer2.shape
print answer3.shape
print answer2
print answer3
