import numpy as np
import string

chars = np.random.choice(list(string.ascii_lowercase), size=10**6, replace=True)
print(chars[:10])


# Dumb solution (but no for loops so hey)
### Edit - actually the question was to match any 4 of the 5 elements. Woops. ########
# This soln tries to match all 5. 
id_chars = np.arange(10**6)

n_waldo = 5 # No of characters in desired string
ok = id_chars < len(id_chars)-n_waldo

ok[ok] = chars[ok]=="w"
index = np.where(ok)[0]
#print "N wa", len(index)

ok[ok] = chars[index+1] == "a"
index = np.where(ok)[0]
#print "N wal", len(index)

ok[ok] = chars[index+2] == "l"
index = np.where(ok)[0]
#print "N wal", len(index)

ok[ok] = chars[index+3] == "d"
index = np.where(ok)[0]
#print "N wald", len(index)

ok[ok] = chars[index+4] == "o"
answer = np.where(ok)[0]
print "N waldo", len(answer)


# Course solution turned out to use as_strided()
# This is an elegant solution, since we are effectively solving the problem by creating many (well 5 at least) copies of the large "chars" array
# But because this is done with as_strided(), each copy is actually a "view" of the original array, so the memory usage is much leaner
# I guess we are still creating a bunch of pointers though (I suppose), so its not like the memory cost will be zero exactly
# Hmm, but if strided is done well, the number of pointers needed could be negligible actually - which is likely the case
# Cool stuff!

print ""
print "Course soln (correctly in this case find cases where 4 of the 5 characters match"
print "The main lesson: read the question carefully!"

byte_size = chars.strides[0] # Number of bytes of each element in "chars"

windows = np.lib.stride_tricks.as_strided(
    x = chars,
    shape = (len(chars) - (5-1), 5), # 5-1 is to account for length of "waldo", 5 in second dimension means we get one column per "waldo" character
    strides = (byte_size, byte_size)
)

waldo = np.array([ "w", "a", "l", "d", "o"], dtype='<U1')

ok = (waldo == windows).astype("int")
n_match = np.sum(ok,axis=1)
answer2 = np.where(n_match >= 4)[0]

print len(answer2)
